// @flow

// Entry point

import * as React from 'react';
import invariant from 'fbjs/lib/invariant';
import { appRoot } from './dom-elements';
import { App } from './app';

const { hot } = module;

// Assert condition to be true
invariant(appRoot != null, 'No root element');

const renderApp = async () => {
  // Split react-dom chunk
  const { default: ReactDOM } = await import('react-dom' /* webpackChunkName: "react-dom", webpackPreload: true */);
  ReactDOM.render(<App />, appRoot);
};

// Hot reloading
if (hot) {
  hot.accept('./app', renderApp);
}

// noinspection JSIgnoredPromiseFromCall
renderApp();
