// https://jestjs.io/docs/en/configuration.html

module.exports = {
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|svg|woff|woff2)$': 'identity-obj-proxy',
    '\\.(css|scss)$': 'identity-obj-proxy',
    '^~/(.*)$': '<rootDir>/src/$1',
  },
};
