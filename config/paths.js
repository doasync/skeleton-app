'use strict';

const fs = require('fs');

const { version } = require('../package.json');

// eslint-disable-next-line security/detect-non-literal-fs-filename
const root = fs.realpathSync(`${__dirname}/..`); // resolve any symlinks etc.

// TODO: get rootUrl from env vars
const rootUrl = 'http://localhost:8080';
const urlPath = `/build/${version}`;

const src = `${root}/src`;
const dist = `${root}/dist${urlPath}`;

const config = `${root}/config`;
const modules = `${root}/node_modules`;
const appModules = `${root}/packages`;

const entryJs = `${src}/app/entry.js`;
const indexHtml = `${src}/index.html`;

const stylelintPattern = '+(**/*.css|**/*.scss)';
const packageJson = `${root}/package.json`;
const env = `${root}/.env`;
const envRef = `${root}/.env.ref`;

const records = `${config}/info/records.json`;

const cssAssetsDirs = ['css', 'assets'];
const injectAssets = [];

const output = {
  css: 'css/[name].css',
  cssChunks: 'css/[id].css',
  assets: '[path][name].[ext]',
  js: 'js/[name].js',
  jsChunks: 'js/[name].js',
  devJs: 'js/[name].js',
  devJsChunks: 'js/[name].js',
};

module.exports = {
  rootUrl,
  urlPath,
  root,
  config,
  modules,
  appModules,
  src,
  dist,
  entryJs,
  indexHtml,
  packageJson,
  output,
  records,
  env,
  envRef,
  stylelintPattern,
  cssAssetsDirs,
  injectAssets,
};
